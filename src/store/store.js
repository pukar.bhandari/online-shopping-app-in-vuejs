import Vue from 'vue';
import Vuex from 'vuex';

import router from '../router';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        idToken: null,
        username: null,
        itemsInCart: [],
        totalAmount: null
    },
    mutations: {
        authUser (state, userData) {
            state.idToken = userData.token;
            state.username = userData.username;
        },
        clearAuthData (state) {
            state.idToken = null;
            state.username = null;
        },
        addItems(state, items) {
            state.itemsInCart.push(items);
        },
        setTotalAmount(state, amount) {
            state.totalAmount = amount;
        }
    },
    actions: {
        addItems({commit},items){
            commit('addItems', items);
        },
        login({commit}, {token, username}) {
            localStorage.setItem('token', token);
            localStorage.setItem('username', username);
            commit('authUser', {
                token: token,
                username: username
            })
        },
        tryAutoLogin({commit}) {
            const token = localStorage.getItem('token');
            const username = localStorage.getItem('username');
            if(!token) {
                return
            }
            commit('authUser', {
                token: token,
                username: username
            })
        },
        logout({commit}) {
            commit('clearAuthData');
            localStorage.removeItem('token');
            localStorage.removeItem('username');
            router.replace('/login');
        }
    },
    getters: {
        userToken(state) {
            return state.idToken;
        },
        username(state) {
            return state.username;
        },
        isAuthenticated(state) {
          return state.idToken !== null
        },
        itemsInCart(state){
            return state.itemsInCart;
        },
        totalAmount(state) {
            return state.totalAmount;
        }
    }
})
