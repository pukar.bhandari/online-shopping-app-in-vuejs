import Vue from 'vue';

import App from './App.vue';

import router from './router';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';

import store from "./store/store";
import Vuelidate from "vuelidate";

import ApiService from "./services/api.service";
import { TokenService } from "./services/token.service";

Vue.config.productionTip = false;

Vue.use(Vuelidate);

ApiService.init('http://localhost:5000/api/v1/');

if(TokenService.getToken()){
  ApiService.setHeader()
}

Vue.filter('dateAgo', function (value) {
  if(!value) return '';
  if (value) {
    const seconds = Math.floor((+new Date() - +new Date(value * 1000)) / 1000);
    if (seconds < 29) // less than 30 seconds ago will show as 'Just now'
      return 'Just now';
    const intervals = {
      'year': 31536000,
      'month': 2592000,
      'week': 604800,
      'day': 86400,
      'hour': 3600,
      'minute': 60,
      'second': 1
    };
    let counter;
    for (const i in intervals) {
      counter = Math.floor(seconds / intervals[i]);
      if (counter > 0)
        if (counter === 1) {
          return counter + ' ' + i + ' ago'; // singular (1 day ago)
        } else {
          return counter + ' ' + i + 's ago'; // plural (2 days ago)
        }
    }
  }
  return value;
})

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
