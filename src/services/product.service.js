import ApiService from "./api.service";
import { from } from "rxjs";

const ProductService= {
    getProducts() {
        return from(ApiService.get('products'));
    },
    getProduct(id) {
      return from(ApiService.get(`/products/${id}`));
    },
    addProduct(product) {
        return from(ApiService.post('products', product));
    },
    addMedia(media, productId) {
        return from(ApiService.put(`products/${productId}/photo`, media));
    },
    updateProduct(product, id) {
        return from(ApiService.put(`products/${id}`, product));
    },
    deleteProduct(id) {
      return from(ApiService.delete(`products/${id}`));
    },
    addToCart(order) {
        return from(ApiService.post('orders', order));
    }
};

export default ProductService;
