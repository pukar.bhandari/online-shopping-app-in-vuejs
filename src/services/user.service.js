import globalAxios from 'axios';
import ApiService from "./api.service";
import { from } from 'rxjs';

const UserService = {
    login(userData) {
        return from(ApiService.post('auth/login', userData));
    },
    register(userData){
        return from(ApiService.post('auth/register', userData));
    },
    getCurrentUser(token) {
        globalAxios.defaults.headers.get['Authorization'] = `Bearer ${token}`;
        return from(ApiService.get('auth/me'));
    },
    forgotPassword(userData) {
        return from(ApiService.post('auth/forgotpassword', userData));
    },
    resetPassword(userData, resetToken) {
        return from(ApiService.put(`auth/resetpassword/${resetToken}`, userData));
    },
    changePassword(userData) {
        return from(ApiService.put('auth/updatepassword', userData));
    },
    updateDetails(userData) {
        return from(ApiService.put('auth/updatedetails', userData));
    },
    getUserOrders(userId) {
        return from(ApiService.get(`orders/${userId}`));
    },
    getUserProducts(userId) {
        return from(ApiService.get(`products/user/${userId}`));
    }
};

export default UserService;
