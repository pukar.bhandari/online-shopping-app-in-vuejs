import store from "../store/store";
import ProductService from "../services/product.service";
import {TokenService} from "@/services/token.service";
import UserService from "@/services/user.service";

export const ProductsResolver = (to, from, next) => {
    store.dispatch('tryAutoLogin');
    if (!store.state.idToken) {
        next('/login')
    }
    if (store.state.idToken) {
        ProductService.getProducts().subscribe(res => {
                to.meta['products'] = res.data.data;
                next();
            },
            err => console.log(err)
        )
    }
}

export const ProductResolver = (to, from, next) => {
    ProductService.getProduct(to.params.id).subscribe(res => {
            to.meta['product'] = res.data.data;
            next();
        },
        err => console.log(err)
    );
}

export const UserProductResolver = (to, from, next) => {
    const token = TokenService.getToken();
    UserService.getCurrentUser(token).subscribe(res => {
            UserService.getUserProducts(res.data.data.id).subscribe(response => {
                to.meta['products'] = response.data.data;
                next();
            })
        },
        err => console.log(err)
    );
}

export const UserOrderResolver = (to, from, next) => {
    const token = TokenService.getToken();
    UserService.getCurrentUser(token).subscribe(res => {
            UserService.getUserOrders(res.data.data.id).subscribe(response => {
                to.meta['orders'] = response.data.data;
                next();
            })
        },
        err => console.log(err)
    );
}