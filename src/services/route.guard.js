import store from "@/store/store";

export const guardMyRoute = (to, from, next) => {
    store.dispatch('tryAutoLogin');
    if(!store.state.idToken) {
        next('/login');
    } if(store.state.idToken) {
        next();
    }
}