const TokenService = {
  getToken() {
      return localStorage.getItem('token');
  }
};

export { TokenService }
