import globalAxios from 'axios';
import { TokenService } from "./token.service";

const ApiService = {

    init(baseURL) {
        globalAxios.defaults.baseURL = baseURL;
    },

    setHeader() {
        globalAxios.defaults.headers.common['Authorization'] = `Bearer ${TokenService.getToken()}`;
    },

    get(url) {
        return globalAxios.get(url);
    },

    post(url, data) {
        return globalAxios.post(url, data);
    },

    put(url, data) {
        return globalAxios.put(url, data);
    },

    delete(url) {
        return globalAxios.delete(url);
    }
};

export default ApiService;
