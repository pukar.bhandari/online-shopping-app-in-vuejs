import Vue from 'vue';
import VueRouter from 'vue-router';

import store from "./store/store";

import Login from "./components/login/Login";
import Register from "./components/register/Register";
import Shopping from "./components/shop/Shopping";
import ProductEdit from './components/shop/ProductEdit';
import ProductDetail from './components/shop/ProductDetail';
import Cart from './components/shop/Cart';
import Checkout from './components/shop/Checkout'
import ForgotPassword from "./components/login/ForgotPassword";
import ResetPassword from "./components/login/ResetPassword";
import UserHome from "./components/user/UserHome";
import MyProfile from "./components/user/MyProfile";
import ChangePassword from "./components/user/ChangePassword";
import EditUserDetails from "./components/user/EditUserDetails";
import UserOrders from "./components/user/UserOrders";
import UserProducts from "./components/user/UserProducts";
import {
    ProductsResolver,
    ProductResolver,
    UserProductResolver,
    UserOrderResolver
} from './services/products.resolver.service';
import {guardMyRoute} from "@/services/route.guard";

Vue.use(VueRouter);

const routes = [
    {
        path: '/login',
        component: Login,
        beforeEnter(to, from, next) {
            store.dispatch('tryAutoLogin');
            if(!store.state.idToken) {
                next()
            } if(store.state.idToken) {
                next('/shop')
            }
        }
    },
    {
      path: '/forgotpassword',
      component: ForgotPassword
    },
    {
        path: '/resetpassword',
        component: ResetPassword
    },
    {
        path: '/register',
        component: Register
    },
    {
      path: '/user/:username',
      component: UserHome,
      name: 'user',
      beforeEnter: guardMyRoute,
      children: [
          {
              path: 'profile',
              component: MyProfile,
              name: 'profile'
          },
          {
              path: 'changepassword',
              component: ChangePassword,
              name: 'changepassword'
          },
          {
              path: 'updatedetails',
              component: EditUserDetails,
              name: 'editUser'
          },
          {
              path: 'orders',
              component: UserOrders,
              name: 'userOrders',
              beforeEnter: UserOrderResolver
          },
          {
              path: 'products',
              component: UserProducts,
              name: 'userProducts',
              beforeEnter: UserProductResolver,
              children: [
                  {
                      path: 'edit/:id',
                      component: ProductEdit,
                      name: 'userProductEdit',
                      beforeEnter: ProductResolver
                  }
              ]
          }
      ]
    },
    {
        path: '/shop',
        component: Shopping,
        name: 'shop',
        beforeEnter: ProductsResolver,
        children: [
            {
              path: 'add',
              component: ProductEdit
            },
            {
                path: 'details/:id',
                component: ProductDetail,
                name: 'detail',
                beforeEnter: ProductResolver
            },
            {
                path: 'cart',
                component: Cart
            },
            {
                path: 'checkout',
                component: Checkout,
                beforeEnter(to, from, next) {
                    if(store.state.itemsInCart.length){
                        next()
                    }
                    if(!store.state.itemsInCart.length){
                        next('/shop/cart')
                    }
                }
            }
        ]
    },
    {
        path: '',
        redirect: '/shop'
    },
    {
        path: '**',
        redirect: '/shop'
    }
];

export default new VueRouter({mode: 'history', routes})
